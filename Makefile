# Rhighlander - a Pale Moon extension to close tabs on the left
# Copyright (C) 2023-2024 Alessio Vanni

# This file is part of Rhighlander.

# Rhighlander is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Rhighlander is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Rhighlander.  If not, see <https://www.gnu.org/licenses/>.

META = install.rdf bootstrap.js chrome.manifest COPYING
LOCALE = locale/en/messages.properties

all: rhighlander.xpi

rhighlander.xpi: $(META) $(LOCALE)
	@zip -r $@ $^

.PHONY: clean

clean:
	@rm -f rhighlander.xpi
