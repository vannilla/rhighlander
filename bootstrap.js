// Rhighlander - a Pale Moon extension to close tabs on the left
// Copyright (C) 2023-2024 Alessio Vanni

// This file is part of Rhighlander.

// Rhighlander is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Rhighlander is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Rhighlander.  If not, see <https://www.gnu.org/licenses/>.

const RhighlanderWindowListener = {
    setup: function (domwindow) {
	'use strict';

	const CU = Components.utils;
	const Services = CU.import('resource://gre/modules/Services.jsm', null).Services;
	const Strings = Services.strings
	      .createBundle('chrome://rhighlander/locale/messages.properties');
	const BrowserStrings = domwindow.gBrowser.mStringBundle;

	const document = domwindow.document;

	const rhighlander = {};
	rhighlander.closeTabs = function (active) {
	    const tabs = [];
	    const visible = domwindow.gBrowser.visibleTabs;

	    for (let i=0; active!=visible[i]; ++i) {
		if (visible[i].pinned) {
		    continue;
		}

		tabs.push(visible[i]);
	    }

	    if (tabs.length > 1 && Services.prefs.getBoolPref('browser.tabs.warnOnCloseOtherTabs')) {
		domwindow.focus();

		const prompt = Services.prompt;
		const title = BrowserStrings.getString("tabs.closeWarningTitle");
		const message = BrowserStrings
		      .getFormattedString("tabs.closeWarningMultipleTabs", [tabs.length]);
		const flags = (prompt.BUTTON_TITLE_IS_STRING * prompt.BUTTON_POS_0)
		      + (prompt.BUTTON_TITLE_CANCEL * prompt.BUTTON_POS_1);

		const result = prompt
		      .confirmEx(domwindow,
				 title,
				 message,
				 flags,
				 BrowserStrings.getString('tabs.closeButtonMultiple'),
				 null,
				 null,
				 null,
				 { value: true });

		if (0 !== result) {
		    // 0 is the index of the OK button
		    return;
		}
	    }

	    for (let t of tabs) {
		domwindow.gBrowser.removeTab(t, { animate: true });
	    }
	};

	domwindow.rhighlander = rhighlander;

	const menuitem = document.createElement('menuitem');
	menuitem.id = 'rhighlander_CloseTabs';
	menuitem.setAttribute('label', Strings.GetStringFromName('menu_label'));
	menuitem.setAttribute('oncommand', 'rhighlander.closeTabs(TabContextMenu.contextTab)');
	menuitem.setAttribute('accesskey', Strings.GetStringFromName('menu_accesskey'));

	document.getElementById('tabContextMenu')
	    .insertBefore(menuitem, document.getElementById('context_closeTabsToTheEnd'));
    },
    teardown: function (domwindow) {
	'use strict';

	const document = domwindow.document;

	document.getElementById('tabContextMenu')
	    .removeChild(document.getElementById('rhighlander_CloseTabs'));
    },

    // Listener interface
    onOpenWindow: function (w) {
	'use strict';

	const CC = Components.classes;
	const CI = Components.interfaces;

	const domwindow = w.QueryInterface(CI.nsIInterfaceRequestor)
	      .getInterface(CI.nsIDOMWindow);

	const onload = function () {
	    if ('navigator:browser' === domwindow.document.documentElement.getAttribute('windowtype')) {
		RhighlanderWindowListener.setup(domwindow);
	    }
	};

	domwindow.addEventListener('load', onload, { getOnce: function () { console.log('once'); return true; } });
    },
    onCloseWindow: function (w) {

    },
    onWindowTitleChange: function (w, title) {

    },
};

function startup(data, reason) {
    const CC = Components.classes;
    const CI = Components.interfaces;

    const wm = CC['@mozilla.org/appshell/window-mediator;1']
	  .getService(CI.nsIWindowMediator);

    const windows = wm.getEnumerator('navigator:browser');
    while (windows.hasMoreElements()) {
	const domwindow = windows.getNext().QueryInterface(CI.nsIDOMWindow);

	RhighlanderWindowListener.setup(domwindow);
    }

    wm.addListener(RhighlanderWindowListener);
}

function shutdown(data, reason) {
    if (APP_SHUTDOWN === reason) {
	return;
    }

    const CC = Components.classes;
    const CI = Components.interfaces;

    const wm = CC['@mozilla.org/appshell/window-mediator;1']
	  .getService(CI.nsIWindowMediator);

    const windows = wm.getEnumerator('navigator:browser');
    for (let w=windows.getNext(); windows.hasMoreElements(); w=windows.getNext()) {
	const domwindow = w.QueryInterface(CI.nsIDOMWindow);

	RhighlanderWindowListener.teardown(domwindow);
    }

    wm.removeListener(RhighlanderWindowListener);
}

function install(data, reason) {
    // Workaround for an old bug; unsure if it has been fixed in UXP,
    // but bootstrapped extensions aren't that great architecturally,
    // so might as well always include this mass of code.
    const CC = Components.classes;
    const CI = Components.interfaces;
    const CU = Components.utils;

    CC['@mozilla.org/intl/stringbundle;1']
	.getService(CI.nsIStringBundleService)
	.flushBundles();

    if (ADDON_UPGRADE === reason) {
	const Services = CU.import('resource://gre/modules/Services.jsm', null).Services;

	Services.obs.notifyObservers(null, 'chrome-flush-caches', null);
	Services.obs.notifyObservers(null, 'message-manager-flush-caches', null);
    }
}

function uninstall(data, reason) {
    // We don't need to do anything here, but let's provide the
    // function anyway as you never know.
}
